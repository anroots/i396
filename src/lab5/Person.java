package lab5;

import java.text.MessageFormat;

public class Person {

	public static final int LEGAL_AGE = 18;
	public static final String GENDER_MALE = "M";
	public static final String GENDER_FEMALE = "F";
	
	private String name;
	private Address address;
	private String gender;
	private int age;

	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		return MessageFormat.format("{0} ({1})", name, age);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Person build() {
		// TODO Auto-generated method stub
		return null;
	}

}
