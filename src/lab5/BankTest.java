package lab5;

import static org.junit.Assert.*;
import lab4.BankService;
import lab4.CardReader;
import lab4.DepositService;
import lab4.DepositSlot;
import lab4.Keypad;
import lab4.Screen;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

public class BankTest {

	private static final String CORRECT_PIN = "123";
	private static final String WRONG_PIN = "421";

	private static final String CORRECT_ACCOUNT_NUMBER = "A456";
	private static final int CORRECT_DEPOSIT_AMOUNT = 150;

	/**
	 * Justification for not doing this in every test method: this Test checks a
	 * very small and specific functionality and @Before resets everything
	 * before every test. Keeps the code... a) DRY b) concise
	 */
	Screen mockScreen;
	BankService mockBankService;
	CardReader cardReader;
	DepositSlot depositSlot;
	Keypad keypad;
	DepositService depositService;

	@Before
	public void setUp() {
		depositService = new DepositService();

		mockScreen = mock(Screen.class);
		depositService.setScreen(mockScreen);

		// BankService
		mockBankService = mock(BankService.class);
		when(mockBankService.getBalance()).thenReturn(0);
		when(mockBankService.isPinCorrect(CORRECT_PIN, CORRECT_ACCOUNT_NUMBER))
				.thenReturn(true);
		depositService.setBankService(mockBankService);

		// Card reader
		cardReader = mock(CardReader.class);
		when(cardReader.getAccountNumber()).thenReturn(CORRECT_ACCOUNT_NUMBER);
		depositService.setCardReader(cardReader);

		// DepositSlot
		depositSlot = mock(DepositSlot.class);
		when(depositSlot.getAmount()).thenReturn(CORRECT_DEPOSIT_AMOUNT);
		depositService.setDepositSlot(depositSlot);
	}

	@Test
	public void depositSuccessScenario() throws Exception {

		// Initialize mocks
		successfulDepositServiceSetup();

		// Execute the system
		depositService.deposit();

		// No messages displayed
		verify(mockScreen, never()).displayMessage(anyString());

		// We checked the PIN and got True
		verify(mockBankService, times(1)).isPinCorrect(CORRECT_PIN,
				CORRECT_ACCOUNT_NUMBER);

		// Some money was deposited
		verify(mockBankService, times(1)).debit(CORRECT_DEPOSIT_AMOUNT,
				CORRECT_ACCOUNT_NUMBER);

		verify(mockBankService, times(1)).debit(CORRECT_DEPOSIT_AMOUNT,
				CORRECT_ACCOUNT_NUMBER);
	}

	@Test
	public void depositFailedWithIncorrectPINScenario() {

		// Initialize mocks
		incorrectPINDepositServiceSetup();

		assertEquals(0, mockBankService.getBalance());

		// Execute the system
		depositService.deposit();

		// Wrong PIN message displayed
		verify(mockScreen, times(1)).displayMessage(Screen.MSG_WRONG_PIN);

		// We checked the PIN and got False
		verify(mockBankService, times(1)).isPinCorrect(WRONG_PIN,
				CORRECT_ACCOUNT_NUMBER);

		// No money was deposited
		verify(mockBankService, never()).debit(anyInt(), anyString());
	}

	private void incorrectPINDepositServiceSetup() {
		keypad = mock(Keypad.class);
		when(keypad.getPin()).thenReturn(WRONG_PIN);
		depositService.setKeypad(keypad);
	}

	/**
	 * Initialize mock components so that they return success values (correct
	 * PIN etc)
	 */
	private void successfulDepositServiceSetup() {
		keypad = mock(Keypad.class);
		when(keypad.getPin()).thenReturn(CORRECT_PIN);
		depositService.setKeypad(keypad);
	}
}
