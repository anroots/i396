= Harjutustund 5 =

_Tähtaeg 7. november 23:59._

== Ülesanne 1 ==

Kirjuta harjutustunni 4 testid ümber nii, et need kasutaksid käsitse tehtud 
mock-ide asemel Mockito teeki. Vajalik teeg asub:
http://mockito.googlecode.com/files/mockito-all-1.9.5.jar

== Ülesanne 2 ==

Builder mustri kasutamine.

Kirjutge testidel põhinevalt (kirjutades testid enne koodi ennast)
klassi PersonUtil meetodid:

* getOldest(p1, p2, p3, p4); - tagastab vanima isiku.
* getPersonsInLegalAge(p1, p2, p3, p4); - tagastab isikud kes on 18+ aastat vanad.
* getWomen(p1, p2, p3, p4); - tagastab naissoost isikud.
* getPersonsWhoLiveIn("Tallinn"); - tagastab isikud, kes elavad Tallinnas.

Testandmete loomiseks kasutada builder mustrit.
Näited failides PersonBuilder.java ja PersonUtilTest.java
