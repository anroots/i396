package lab5;

public class PersonBuilder {

	private String name;
	private AddressBuilder addressBuilder;
	private String gender;
	private int age;

	public PersonBuilder() {
		addressBuilder = new AddressBuilder();
		age = 21;
		gender = Person.GENDER_FEMALE;
		name = "Mari";
	}

	public PersonBuilder withAge(int age) {
		this.age = age;
		return this;
	}

	public Person build() {
		Person person = new Person(name, age);
		person.setAddress(addressBuilder.build());
		person.setGender(gender);

		return person;
	}

	public PersonBuilder male() {
		gender = Person.GENDER_MALE;
		return this;
	}

	public PersonBuilder inTown(String town) {
		addressBuilder.inTown(town);
		return this;
	}

}
