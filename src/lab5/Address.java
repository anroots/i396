package lab5;

public class Address {

	private String building;
	private String street;
	private String town;

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	@Override
	public boolean equals(Object obj) {
		Address address = (Address) obj;

		return address.getBuilding().equals(getBuilding())
				&& address.getStreet().equals(getStreet())
				&& address.getTown().equals(getTown());
	}

}
