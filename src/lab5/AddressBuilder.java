package lab5;

public class AddressBuilder {

	private String town = "Tallinn";
	private String street = "Raja";
	private String building = "4C";

	public AddressBuilder inTown(String town) {
		this.town = town;
		return this;
	}

	public Address build() {
		Address address = new Address();
		address.setTown(town);
		address.setStreet(street);
		address.setBuilding(building);
		return address;
	}

}
