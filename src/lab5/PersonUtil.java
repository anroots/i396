package lab5;

import java.util.ArrayList;
import java.util.List;

public class PersonUtil {

	/**
	 * Return the oldest person in the list of persons
	 * 
	 * @param persons
	 * @return
	 */
	public Person getOldest(List<Person> persons) {
		if (persons.size() == 0) {
			return null;
		}

		Person oldest = persons.get(0);

		for (Person person : persons) {
			if (person.getAge() > oldest.getAge()) {
				oldest = person;
			}
		}

		return oldest;
	}

	/**
	 * Return only persons whose age is greater than Person.LEGAL_AGE
	 * 
	 * @param persons
	 * @return
	 */
	public List<Person> getPersonsInLegalAge(List<Person> persons) {
		List<Person> inLegalAge = new ArrayList<Person>();

		if (persons.size() == 0) {
			return null;
		}

		for (Person person : persons) {
			if (person.getAge() >= Person.LEGAL_AGE) {
				inLegalAge.add(person);
			}
		}

		return inLegalAge;
	}

	/**
	 * Return only women
	 * 
	 * @param persons
	 * @return
	 */
	public List<Person> getWomen(List<Person> persons) {
		List<Person> women = new ArrayList<Person>();

		for (Person person : persons) {
			if (person.getGender().equals(Person.GENDER_FEMALE)) {
				women.add(person);
			}
		}

		return women;
	}

	/**
	 * Return only persons who live in the given address.
	 * Justification for changing the given signature: needed a way to give in a list of persons
	 * 
	 * @param address
	 * @param persons
	 * @return
	 */
	public List<Person> getPersonsWhoLiveIn(Address address,
			List<Person> persons) {
		List<Person> personsInLocation = new ArrayList<Person>();

		for (Person person : persons) {
			if (person.getAddress().equals(address)) {
				personsInLocation.add(person);
			}
		}
		return personsInLocation;
	}

}