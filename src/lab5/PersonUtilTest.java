package lab5;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class PersonUtilTest {

	private PersonUtil personUtil = new PersonUtil();

	@Test
	public void oldestPersonIsReturned() {

		Person p1 = aPerson().withAge(32).build();
		Person p2 = aPerson().withAge(16).build();
		Person p3 = aPerson().withAge(55).build();
		Person p4 = aPerson().withAge(18).build();

		assertThat(p3, is(personUtil.getOldest(Arrays.asList(p1, p2, p3, p4))));
	}

	@Test
	public void thereIsNoOldestPersonInAnEmptyList() {
		List<Person> emptyList = new ArrayList<Person>();
		assertNull(personUtil.getOldest(emptyList));
	}

	@Test
	public void onlyLegalAgedPersonsAreReturned() {
		Person p1 = aPerson().withAge(32).build();
		Person p2 = aPerson().withAge(16).build();
		Person p3 = aPerson().withAge(55).build();
		Person p4 = aPerson().withAge(18).build();

		List<Person> legalAged = personUtil.getPersonsInLegalAge(Arrays.asList(
				p1, p2, p3, p4));

		assertEquals(3, legalAged.size());
		assertFalse(legalAged.contains(p2));
	}

	@Test
	public void onlyWomenAreReturned() {
		Person p1 = aPerson().build();
		Person p2 = aPerson().male().build();
		Person p3 = aPerson().male().build();
		Person p4 = aPerson().build();

		List<Person> women = personUtil.getWomen(Arrays.asList(p1, p2, p3, p4));

		assertEquals(2, women.size());

		assertFalse(women.contains(p2));
		assertFalse(women.contains(p3));

		assertTrue(women.contains(p1));
		assertTrue(women.contains(p4));
	}

	@Test
	public void onlyPersonsLivingInTallinnAreReturned() {

		Person p1 = aPerson().build();
		Person p2 = aPerson().inTown("Haapsalu").build();
		Person p3 = aPerson().inTown("Tartu").build();
		Person p4 = aPerson().inTown("Tallinn").build();

		Address addressInTallinn = new AddressBuilder().inTown("Tallinn")
				.build();

		List<Person> tallinnlanders = personUtil.getPersonsWhoLiveIn(
				addressInTallinn, Arrays.asList(p1, p2, p3, p4));

		assertEquals(2, tallinnlanders.size());

		assertTrue(tallinnlanders.contains(p1));
		assertTrue(tallinnlanders.contains(p4));

		assertFalse(tallinnlanders.contains(p2));
		assertFalse(tallinnlanders.contains(p3));
	}

	private PersonBuilder aPerson() {
		return new PersonBuilder();
	}

}
