package lab3;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import lab3.Refactoring.Order;

import org.junit.Before;
import org.junit.Test;

public class RefactoringTest {

	private Refactoring ref;

	@Before
	public void setUp() throws Exception {
		ref = new Refactoring();
	}

	@Test
	public void testGetIncreasedCount() {
		assertThat(ref.getIncreasedCount(), is(0));
	}

	@Test
	public void testaddFilledOrdersTo() {
		List<Order> filledOrders = new ArrayList<Order>();
		ref.addFilledOrdersTo(filledOrders);
		assertThat(filledOrders.size(), is(0));
	}

	@Test
	public void testGetItemsAsHtml() {
		String html = ref.getItemsAsHtml();
		assertThat(html,
				is("<ul><li>1</li><li>2</li><li>3</li><li>4</li></ul>"));
	}

	@Test
	public void testCalculateWeeklyPayWithoutOvertime() {
		int pay1 = ref.calculateWeeklyPay(false);
		assertThat(pay1, is(225));
	}

	@Test
	public void testCalculateWeeklyPayWithOvertimeParam() {
		int pay1 = ref.calculateWeeklyPay(true);
		assertThat(pay1, is(238));
	}

	@Test
	public void testCalculateStraightWeeklyPay() {
		int pay1 = ref.calculateStraightWeeklyPay();
		assertThat(pay1, is(225));
	}

	@Test
	public void testCalculateWeeklyPayWithOvertime() {
		int pay1 = ref.calculateWeeklyPayWithOvertime();
		assertThat(pay1, is(238));
	}
}
