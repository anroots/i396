package lab3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Refactoring {

	private static final double NORMAL_RATE = 1.0;
	private static final double OVERTIME_RATE = 1.5;
	private static final int NOMINAL_WEEKLY_WORK_HOURS = 40;
	private static final double VAT = 1.2;

	// 1a
	public int getIncreasedCount() {
		return count++;
	}

	// 1b
	public void addFilledOrdersTo(List<Order> o) {
		for (Order order : orders) {
			if (order.isFilled()) {
				o.add(order);
			}
		}
	}

	// 2a
	public void printStatementFor(Long invoiceId) {
		List<InvoiceRow> invoiceRows = dao.getInvoiceRowsFor(invoiceId);

		printInvoiceRows(invoiceRows);

		double t = calculateInvoiceTotal(invoiceRows);
		printValue(t);
	}

	private double calculateInvoiceTotal(List<InvoiceRow> invoiceRows) {
		double t = 0;
		for (InvoiceRow invoiceRow : invoiceRows) {
			t += invoiceRow.getAmount();
		}
		return t;
	}

	// 2b
	public String getItemsAsHtml() {

		DOMObject ul = new DOMObject("ul");
		List<String> elementsToAdd = new ArrayList<String>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add(item1);
				add(item2);
				add(item3);
				add(item4);
			}
		};

		for (String element : elementsToAdd) {
			DOMObject li = new DOMObject("li");
			li.addChild(new DOMObject(element));
			ul.addChild(li);
		}
		return ul.toString();
	}

	// 3
	public boolean isSmallOrder() {
		return order.getTotal() > 100;
	}

	// 4
	public void printPrice() {
		System.out.println("Hind ilma k�ibemaksuta: " + getBasePrice());
		System.out.println("Hind k�ibemaksuga: " + getBasePriceWithVAT());
	}

	private Double getBasePriceWithVAT() {
		return getBasePrice() * VAT;
	}

	// 5
	public void isOutOfCanvas(Shape shape) {

		// True if one of the 2-dimensions is bigger than the length of the
		// canvas in that dimension
		boolean coordinateOutOfBounds = shape.x > 1000 || shape.y > 500;

		// out of canvas, shape is square
		if (coordinateOutOfBounds && shape.width == shape.height
				&& shape.curvature == 0) {
			// ...
		}

		// ...
	}

	// 6
	public boolean canAccessResource(SessionData sessionData) {

		return isAdmin(sessionData) && preferedStatusSet(sessionData);
	}

	private boolean preferedStatusSet(SessionData sessionData) {
		return sessionData.getStatus().equals("preferredStatusX")
				|| sessionData.getStatus().equals("preferredStatusY");
	}

	private boolean isAdmin(SessionData sessionData) {
		return sessionData.getCurrentUserName().equals("Admin")
				|| sessionData.getCurrentUserName().equals("Administrator");
	}

	// 7
	public void drawLines() {
		Space space = new Space();

		Point3D start = new Point3D(12, 3, 5);
		Point3D end = new Point3D(2, 4, 6);
		space.drawLine(start, end);

		start = new Point3D(2, 4, 6);
		end = new Point3D(0, 1, 0);
		space.drawLine(start, end);
	}

	// 8
	public int calculateWeeklyPay(boolean overtime) {
		return overtime ? calculateWeeklyPayWithOvertime()
				: calculateStraightWeeklyPay();
	}

	public int calculateStraightWeeklyPay() {
		double overtimeRate = NORMAL_RATE * hourRate;
		int overtimePay = getOverTimePay(overtimeRate);
		return getStraightPay() + overtimePay;
	}

	private int getOverTimePay(double overtimeRate) {

		return (int) Math.round(getOverTime() * overtimeRate);
	}

	private int getStraightPay() {
		return getStraightTime() * hourRate;
	}

	private int getOverTime() {
		return Math.max(0, hoursWorked - getStraightTime());
	}

	private int getStraightTime() {
		return Math.min(NOMINAL_WEEKLY_WORK_HOURS, hoursWorked);
	}

	public int calculateWeeklyPayWithOvertime() {
		double overtimeRate = OVERTIME_RATE * hourRate;
		return getStraightPay() + getOverTimePay(overtimeRate);
	}

	// //////////////////////////////////////////////////////////////////////////

	// Abiv�ljad ja abimeetodid.
	// Need on siin lihtsalt selleks, et kood kompileeruks

	private final String item1 = "1";
	private final String item2 = "2";
	private final String item3 = "3";
	private final String item4 = "4";
	private final int hoursWorked = 45;
	private final int hourRate = 5;
	int count = 0;
	private final List<Order> orders = new ArrayList<Order>();
	private final Order order = new Order();
	private final Dao dao = new SampleDao();
	private final double price = 0;

	void justACaller() {
		getIncreasedCount();
		addFilledOrdersTo(null);
	}

	private void printValue(double total) {
	}

	private void printInvoiceRows(List<InvoiceRow> invoiceRows) {
	}

	class Space {
		public void drawLine(Point3D start, Point3D end) {
		}

	}

	class Point3D {
		private final int x;
		private final int y;
		private final int z;

		Point3D(int x, int y, int z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}

	interface Dao {
		List<InvoiceRow> getInvoiceRowsFor(Long invoiceId);
	}

	class SampleDao implements Dao {
		@Override
		public List<InvoiceRow> getInvoiceRowsFor(Long invoiceId) {
			return null;
		}
	}

	class Order {
		public boolean isFilled() {
			return false;
		}

		public double getTotal() {
			return 0;
		}
	}

	class InvoiceRow {
		public double getAmount() {
			return 0;
		}
	}

	/**
	 * This class is a semi-valid way of avoiding repetition when building HTML.
	 * 
	 * We'd normally use either... a) A more sophisticated class with better
	 * interface design b) A templating system
	 * 
	 * Neither of those is worth the trouble of implementation for this lab
	 * assignment.
	 */
	class DOMObject {

		String name;
		String textContent;
		List<DOMObject> children = new LinkedList<DOMObject>();

		private final List<String> validElements = new ArrayList<String>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add("ul");
				add("li");
			}
		};

		DOMObject(String name) {
			if (validElements.contains(name)) {
				this.name = name;
			} else {
				this.textContent = name;
			}

		}

		public void addChild(DOMObject child) {
			children.add(child);
		}

		// Todo: Use Stringbuilder
		@Override
		public String toString() {

			if (textContent == null) {
				return toHTML();
			}
			return textContent;

		}

		private String toHTML() {
			String childHTML = "";
			if (children.size() > 0) {
				for (DOMObject child : children) {
					childHTML += child.toString();
				}
			}
			return "<" + name + ">" + childHTML + "</" + name + ">";
		}
	}

	class Shape {

		public int y;
		public int x;
		public int curvature;
		public Object height;
		public Object width;
	}

	private double getBasePrice() {
		return price;
	}

	private class SessionData {

		public String getCurrentUserName() {
			return null;
		}

		public String getStatus() {
			return null;
		}

	}

}
