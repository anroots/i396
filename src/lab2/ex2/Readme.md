Kirjuatada klass RpnCalculator (Rpn - pööratud Poola notatsioon),
kirjutades testid enne koodi.

infoks:
http://www.alcula.com/calculators/rpn/
http://en.wikipedia.org/wiki/Reverse_Polish_notation

RpnCalculator-il on meetodid: 
  int getAccumulator() - tagastab akumulaatoris oleva väärtuse
  void setAccumulator(int number) - seab akumulaatori väärtuse
  void enter() - paneb akumulaatori pinusse
  void plus() - eemaldab pinust pealmise elemendi, liidab sellele akumulaatori
    ja paneb tulemuse akumulaatorisse
  void multiply() - eemaldab pinust pealmise elemendi, korrutab sellele 
    akumulaatoriga ja paneb tulemuse akumulaatorisse

Sisemise andmestruktuurina kasutada  java.util.Stack-i

vajalikud on vähemalt järgmised testid:

- loo kalkulaator ja vaata et akumulaator on 0
- kontrolli, et on võimalik akumulaatorit seada
- kontrolli, et kalkulaator arvutab õigesti 1 + 2 = 3
  näide:
	RpnCalculator c = new RpnCalculator();
	c.setAcumulator(1);
	c.enter();
	c.setAcumulator(2);
	c.plus();
	assertThat(c.getAcumulator(), is(3));
- kontrolli, et kalkulaator arvutab õigesti (1 + 2) * 3 = 9
- kontrolli, et kalkulaator arvutab õigesti (1 + 2) * (3 + 4) = 21

Kui kui üldse aru ei saa, mida tegema peab, siis siin on näide:
https://vimeo.com/10569751
