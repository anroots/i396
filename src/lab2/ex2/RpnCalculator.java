package lab2.ex2;

import java.util.Stack;

/**
 * Basic RPN calculator class.
 * 
 * @author Ando Roots
 * 
 */
public class RpnCalculator {

	/**
	 * Holds the entered integer values in memory.
	 */
	private final Stack<Integer> stack = new Stack<Integer>();

	/**
	 * The current, visible number. Think traditional scientific calculator
	 * display.
	 */
	private int accumulator = 0;

	/**
	 * For testing only, look at the topmost number in stack
	 * 
	 * @return
	 */
	public final int peekStack() {
		return stack.peek();
	}

	public int popStack() {
		return stack.pop();
	}

	/**
	 * 
	 * @return
	 */
	public int getAccumulator() {
		return accumulator;
	}

	/**
	 * 
	 * @param number
	 */
	public void setAccumulator(int number) {
		accumulator = number;
	}

	/**
	 * Enter the visible value to memory. This must be pressed after each
	 * numerical entry (you cannot press operands before pressing enter)
	 */
	public void enter() {
		stack.add(getAccumulator());
		accumulator = 0;
	}

	public void plus() {
		if (stack.size() < 2) {
			throw new NullPointerException();
		}

		int operand1 = popStack();
		int operand2 = popStack();
		int answer = operand1 + operand2;
		setAccumulator(answer);
		enter();
	}

	public void multiply() {
		if (stack.size() < 2) {
			throw new NullPointerException();
		}

		int operand1 = popStack();
		int operand2 = popStack();
		int answer = operand1 * operand2;
		setAccumulator(answer);
		enter();
	}
}
