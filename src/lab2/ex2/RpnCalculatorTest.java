/**
 * 
 */
package lab2.ex2;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

/**
 * Test class for RPN calculator.
 * 
 * Note: Comments given in the previous lecture by the instructor have been
 * taken into account in test method naming: drop _ (dash) convention, use
 * descriptive names instead.
 * 
 * @author Ando Roots
 * 
 */
public class RpnCalculatorTest {

	private RpnCalculator calc;

	@Before
	public void setUp() {
		calc = new RpnCalculator();
	}

	/**
	 * Test method for {@link lab2.ex2.RpnCalculator#RpnCalculator()}.
	 */
	@Test
	public void testRpnCalculatorCreation() {
		assertThat(calc.getAccumulator(), is(0));
	}

	/**
	 * Test method for {@link lab2.ex2.RpnCalculator#getAccumulator()}.
	 */
	@Test
	public void testGetAccumulator() {
		int num = 2;
		calc.setAccumulator(num);
		assertThat(calc.getAccumulator(), is(num));
	}

	/**
	 * Test method for {@link lab2.ex2.RpnCalculator#setAccumulator(int)}.
	 */
	@Test
	public void testSetAccumulatorValue() {
		int num = 1;
		calc.setAccumulator(num);
		assertThat(calc.getAccumulator(), is(num));
	}

	/**
	 * Test method for {@link lab2.ex2.RpnCalculator#enter()}.
	 */
	@Test
	public void testEnterAddsToStack() {
		int num = 1;
		calc.setAccumulator(num);
		calc.enter();
		assertThat(calc.peekStack(), is(num));
	}

	@Test
	public void testMultiplyByZero() {
		int num1 = 0;
		int num2 = 2;
		calc.setAccumulator(num1);
		calc.enter();
		calc.setAccumulator(num2);
		calc.enter();
		calc.multiply();
		int answer = calc.peekStack();

		assertThat(answer, is(0));
	}

	@Test
	public void testEnterTwoDigitNumbers() {
		int num = 88;
		calc.setAccumulator(num);
		calc.enter();
		assertThat(calc.peekStack(), is(num));
	}

	@Test
	public void testEnterResetsAccumulator() {
		int num = 1;
		calc.setAccumulator(num);
		calc.enter();
		assertThat(calc.getAccumulator(), is(0));
	}

	@Test(expected = NullPointerException.class)
	public void testPlusNoOperandsThrows() {
		calc.plus();
	}

	@Test(expected = NullPointerException.class)
	public void testPlusOneOperandThrows() {
		calc.setAccumulator(4);
		calc.enter();
		calc.plus();
	}

	@Test(expected = NullPointerException.class)
	public void testMultiplyNoOperandsThrows() {
		calc.multiply();
	}

	@Test(expected = NullPointerException.class)
	public void testMultiplyOneOperandThrows() {
		calc.setAccumulator(4);
		calc.enter();
		calc.multiply();
	}

	/**
	 * Test method for {@link lab2.ex2.RpnCalculator#plus()}.
	 */
	@Test
	public void testPlus() {
		calc.setAccumulator(1);
		calc.enter();
		calc.setAccumulator(2);
		calc.enter();
		calc.plus();
		assertThat(calc.peekStack(), is(3));
	}

	/**
	 * Test method for {@link lab2.ex2.RpnCalculator#multiply()}.
	 */
	@Test
	public void testMultiply() {
		calc.setAccumulator(3);
		calc.enter();
		calc.setAccumulator(2);
		calc.enter();
		calc.multiply();
		int calcAnswer = calc.peekStack();
		assertThat(calcAnswer, is(6));
	}

	/**
	 * (1 + 2) * 3 = 9
	 */
	@Test
	public void testCalculation2() {
		calc.setAccumulator(1);
		calc.enter();
		calc.setAccumulator(2);
		calc.enter();
		calc.plus();
		calc.setAccumulator(3);
		calc.enter();
		calc.multiply();
		assertThat(calc.peekStack(), is(9));
	}

	/**
	 * (1 + 2) * (3 + 4) = 21
	 */
	@Test
	public void testCalculation3() {

		calc.setAccumulator(1);
		calc.enter();
		calc.setAccumulator(2);
		calc.enter();
		calc.plus();
		calc.setAccumulator(3);
		calc.enter();
		calc.setAccumulator(4);
		calc.enter();
		calc.plus();
		calc.multiply();

		assertThat(calc.peekStack(), is(21));

	}
}
