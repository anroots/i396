package lab2.ex1;

public class Ex1 {
	
	public static int fun1(int x, int y) {
		int z = 0;
		if ((x > 0) && (y > 0)) {
			z = x;
		}
		return z;
	}

	public static Object[] reverse(Object[] a) {
		if (a == null)
			return null;
		Object[] b = new Object[a.length];
		for (int i = 0; i < a.length; i++) {
			b[i] = a[a.length - 1 - i];
		}
		return b;
	}
}
