package lab2.ex1;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class Ex1Test {

	@Test
	public void fun1ReturnsZero() {
		int answer1 = Ex1.fun1(0, 0);
		assertThat(0, is(answer1));
	}

	@Test
	public void fun1ReturnsFirst_FirstIsPositive() {
		int answer2 = Ex1.fun1(3, 4);
		assertThat(3, is(answer2));
	}
	
	@Test
	public void fun1ReturnsZero_FirstIsNegative() {
		int answer1 = Ex1.fun1(-3, 4);
		assertThat(0, is(answer1));
	}

	@Test
	public void fun1ReturnsZero_SecondIsNegative() {
		int answer1 = Ex1.fun1(3, -4);
		assertThat(0, is(answer1));
	}
	
	@Test
	public void reverseReturnsNull() {
		assertThat(null, is(Ex1.reverse(null)));
	}

	@Test
	public void reverseReturnsObjects() {
		Ex1[] objects = new Ex1[3];

		for (int i = 0; i < 3; i++) {
			objects[i] = new Ex1();
		}

		Object[] reversed = Ex1.reverse(objects);

		assertSame(objects[0],	reversed[2]);
		assertSame(objects[1],	reversed[1]);
		assertSame(objects[2],	reversed[0]);
	}

}
