package lab4;

public interface Screen {

	/**
	 * Live system would have some kind of a I18n layer here.
	 */
	public static final String MSG_WRONG_PIN = "Wrong PIN";

	void displayMessage(String message);

}
