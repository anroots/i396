package lab4;

public interface BankService {

	boolean isPinCorrect(String pin, String accountNumber);
	
	void debit(int amount, String accountNumber);
	
	public int getBalance();
}
