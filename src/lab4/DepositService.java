package lab4;

public class DepositService {

	private Screen screen;
	private Keypad keypad;
	private CardReader cardReader;
	private BankService bankService;
	private DepositSlot depositSlot;

	public void deposit() {
		String pin = keypad.getPin();

		String accountNumber = cardReader.getAccountNumber();

		if (!bankService.isPinCorrect(pin, accountNumber)) {
			screen.displayMessage(Screen.MSG_WRONG_PIN);
			return;
		}

		int depositAmount = depositSlot.getAmount();
		bankService.debit(depositAmount, accountNumber);

	}

	public void setScreen(Screen screen) {
		this.screen = screen;
	}

	public void setKeypad(Keypad keypad) {
		this.keypad = keypad;
	}

	public void setCardReader(CardReader cardReader) {
		this.cardReader = cardReader;
	}

	public void setBankService(BankService database) {
		this.bankService = database;
	}

	public void setDepositSlot(DepositSlot depositSlot) {
		this.depositSlot = depositSlot;
	}
}
