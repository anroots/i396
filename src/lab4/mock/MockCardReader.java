package lab4.mock;

import lab4.CardReader;

public class MockCardReader implements CardReader {

	private String accountNumber;

	public MockCardReader(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@Override
	public String getAccountNumber() {
		return accountNumber;
	}

}
