package lab4.mock;

import lab4.BankService;

public class MockBankService implements BankService {

	private String correctPin;
	private String correctAccountNumber;

	private String inputPin;
	private String inputAccountNumber;

	private int debitInputAmount = 0;
	private String debitInputAccountNumber;

	private int balance = 0;

	public MockBankService(String correctPin, String correctAccountNumber) {
		this.correctAccountNumber = correctAccountNumber;
		this.correctPin = correctPin;
	}

	public int getBalance() {
		return balance;
	}

	@Override
	public boolean isPinCorrect(String pin, String accountNumber) {

		// Save input values for auditing
		inputPin = pin;
		inputAccountNumber = accountNumber;

		return pin.equals(correctPin)
				&& accountNumber.equals(correctAccountNumber);
	}

	public boolean wasDebitCalledWith(int amount, String accountNumber) {
		return debitInputAccountNumber.equals(accountNumber)
				&& debitInputAmount == amount;
	}

	@Override
	public void debit(int amount, String accountNumber) {
		debitInputAccountNumber = accountNumber;
		debitInputAmount = amount;
		balance += amount;
	}

	public boolean wasIsPinCorrectCalledWith(String correctPin,
			String correctAccountNumber) {
		return correctAccountNumber.equals(inputAccountNumber)
				&& correctPin.equals(inputPin);
	}

	public boolean wasDebitCalled() {
		return debitInputAccountNumber != null || debitInputAmount != 0;
	}

}
