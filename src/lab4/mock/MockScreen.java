package lab4.mock;

import java.util.ArrayList;
import java.util.List;

import lab4.Screen;

public class MockScreen implements Screen {

	private List<String> displayedMessages = new ArrayList<String>();

	public boolean wasMessageDisplayed(String message) {
		return displayedMessages.contains(message);
	}

	public int getMessageCount() {
		return displayedMessages.size();
	}

	@Override
	public void displayMessage(String message) {
		displayedMessages.add(message);
	}

}
