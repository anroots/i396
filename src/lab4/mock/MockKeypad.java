package lab4.mock;

import lab4.Keypad;

public class MockKeypad implements Keypad {

	private String pin;

	public MockKeypad(String pin) {
		this.pin = pin;
	}

	@Override
	public String getPin() {
		return pin;
	}

}
