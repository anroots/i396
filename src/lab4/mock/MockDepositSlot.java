package lab4.mock;

import lab4.DepositSlot;

public class MockDepositSlot implements DepositSlot {

	private int depositAmount;

	public MockDepositSlot(int correctDepositAmount) {
		depositAmount = correctDepositAmount;
	}


	@Override
	public int getAmount() {
		return depositAmount;
	}

}
