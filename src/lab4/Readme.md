Harjutustund 4.

Harjutustunni eesmärk on mock objektide kasutamine. Sügavama arusaamise huvides
kirjutage seekord mock-id käsitsi. Käsitsi tehtud mock-i näide on loengu slaidil
31.

Süsteem koosneb peaklassist DepositService, mis oma tööks
kasutab alamsüsteeme, mis on liidesega eraldatud.

Realiseerida kasutuslugu:

  UC1 - "Sularaha sissemakse"
  1. süsteem küsib kasutajalt PIN-i
  2. süsteem loeb kaardilt konto numbri
  3. süsteem kontrollib pangateenuselt, kas PIN on õige
     3a kui PIN on vale siis näidata sõnumit "Wrong PIN"
     3b kui PIN on õige jätkata peavoo punktist 4
  4. süsteem küsib sisestusseadmelt (DepositSlot), mis summa sisestati.
  5. süsteem kannab raha kasutaja arvele

Kasutuslugu asub klassis DepositService ja meetodis deposit().

Kontrollida järgmisi tsenaariume:

1. kõik õnnestub (Happy path).
   Veenduda, et debit() meetodit kutsuti välja sobivate argumentidega.
   
2. kasutaja sisestas vale PIN-i.
   Veenduda, et ekraanile saadeti veasõnum ja debit() meetodit välja ei kutsutud.

Ette on antud süsteemi erinevate osade liidesed.
Teie peate neile liidestele kirjutama (käsitsi ilma raamistiku kasutamata) mock objektid.
Kasutades neid mock-e kirjutada testid mõlemale tsenaariumile.

Ette on antud:

DepositService.deposit() - meetod, mis realiseerib antud kasutusloo
                           See on poolik meetod, mille peate lõpuni kirjutama.
BankTest - testiklass, mille peate lõpuni kirjutama.
CardReader, DepositSlot, Keypad, Screen, BankService - liidesed, mida antud kasutuslugu kasutab
ja millele peate mock-id kirjutama.

Juurde tuleb teha mock-id, mis implementeerivad nimetatud liideseid:

MockScreen - kontrollib kas meetodit displayMessage() kutsuti välja.
MockBankService - kontrollib meetodeid isPinCorrect() ja debit().
  Konstruktoris anda sisse, milline on õige PIN-i ja kontonumbri kombinatsioon.
  isPinCorrect kasutab neid konstruktoris antud väärtusi oma vastuse koostamiseks -
  kui need väärtused langevad meetodi argumentidega kokku, siis tagastatakse true muidu false.
  MockBankService omab seega kuute välja. Kaks õige PIN-i ja kontonumbri kombinatsiooni
  hoidmiseks, kaks lipukeste jaoks mis ütlevad, kas meetodeid isPinCorrect() ja debit()
  välja kutsuti ja kaks debit() meetodi väljakutsel antud argumentide jaoks.

MockKeypad - tema getPin() meetod väljastab ette antud PIN-i 
             (Oodatava PIN-i annab konstruktoris sisse). On lihtsalt stub-i osas.
MockCardReader - tema getAccountNumber() meetod väljastab ette antud arve numbri 
             (Oodatava arve numbri annab konstruktoris sisse). On lihtsalt stub-i osas.
MockDepositSlot - kontrollib kas meetodit getAmount() kutsuti välja.

Meenutus loengust:

01 public void transferMoney(int amount, 
02 	String formAccount, String toAccount) {
03 	
04 	if (amount <= 0 || formAccount.equals(toAccount)) return;
05 	
06 	if (bankService.getBalance(formAccount) >= amount) {
07 		bankService.transfer(amount, formAccount, toAccount);
08 	}
09 }

seda kas realt 4 tagasi pöördutakse saame teda selle järgi, kas 
bankService.getBalance() meetodit välja kutsuti.
