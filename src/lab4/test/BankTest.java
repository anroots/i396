package lab4.test;

import static org.junit.Assert.*;
import lab4.CardReader;
import lab4.DepositService;
import lab4.DepositSlot;
import lab4.Keypad;
import lab4.mock.MockBankService;
import lab4.mock.MockCardReader;
import lab4.mock.MockDepositSlot;
import lab4.mock.MockKeypad;
import lab4.mock.MockScreen;

import org.junit.Before;
import org.junit.Test;

public class BankTest {

	private static final String CORRECT_PIN = "123";
	private static final String WRONG_PIN = "421";

	private static final String CORRECT_ACCOUNT_NUMBER = "A456";
	private static final int CORRECT_DEPOSIT_AMOUNT = 150;

	/**
	 * Justification for not doing this in every test method: this Test checks a
	 * very small and specific functionality and @Before resets everything
	 * before every test. Keeps the code... a) DRY b) concise
	 */
	MockScreen mockScreen;
	MockBankService mockBankService;
	CardReader cardReader;
	DepositSlot depositSlot;
	Keypad keypad;
	DepositService depositService;

	@Before
	public void setUp() {
		depositService = new DepositService();

		mockScreen = new MockScreen();
		depositService.setScreen(mockScreen);

		mockBankService = new MockBankService(CORRECT_PIN,
				CORRECT_ACCOUNT_NUMBER);
		depositService.setBankService(mockBankService);

		cardReader = new MockCardReader(CORRECT_ACCOUNT_NUMBER);
		depositService.setCardReader(cardReader);

		depositSlot = new MockDepositSlot(CORRECT_DEPOSIT_AMOUNT);
		depositService.setDepositSlot(depositSlot);
	}

	@Test
	public void depositSuccessScenario() throws Exception {

		// Initialize mocks
		successfulDepositServiceFactory();

		// Initial balance
		assertEquals(0, mockBankService.getBalance());

		// Execute the system
		depositService.deposit();

		// No messages displayed
		assertEquals(0, mockScreen.getMessageCount());

		// We checked the PIN and got True
		assertTrue(mockBankService.wasIsPinCorrectCalledWith(CORRECT_PIN,
				CORRECT_ACCOUNT_NUMBER));
		
		// Some money was deposited
		assertEquals(CORRECT_DEPOSIT_AMOUNT, mockBankService.getBalance());
		assertTrue(mockBankService.wasDebitCalledWith(CORRECT_DEPOSIT_AMOUNT, CORRECT_ACCOUNT_NUMBER));
	}

	@Test
	public void depositFailedWithIncorrectPINScenario() {

		// Initialize mocks
		incorrectPINDepositServiceFactory();

		assertEquals(0, mockBankService.getBalance());

		// Execute the system
		depositService.deposit();

		// Wrong PIN message displayed
		assertEquals(1, mockScreen.getMessageCount());
		assertTrue(mockScreen.wasMessageDisplayed(MockScreen.MSG_WRONG_PIN));

		// We checked the PIN and got False
		assertTrue(mockBankService.wasIsPinCorrectCalledWith(WRONG_PIN,
				CORRECT_ACCOUNT_NUMBER));

		assertFalse(mockBankService.wasDebitCalled());

		// No money was deposited
		assertEquals(0, mockBankService.getBalance());
	}

	private void incorrectPINDepositServiceFactory() {
		keypad = new MockKeypad(WRONG_PIN);
		depositService.setKeypad(keypad);
	}

	/**
	 * Initialize mock components so that they return success values (correct
	 * PIN etc)
	 */
	private void successfulDepositServiceFactory() {
		keypad = new MockKeypad(CORRECT_PIN);
		depositService.setKeypad(keypad);
	}
}
