package lab6;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order {

	private boolean filled = false;
	private String number;
	private Date date;
	private List<LineItem> items = new ArrayList<LineItem>();

	public Order(String number, Date date) {
		this.number = number;
		this.date = date;
	}

	public void addItem(LineItem l1) {
		items.add(l1);
	}

	public List<LineItem> getLineItems() {
		return null;
	}

	public double getTotal() {
		double total = 0.0;
		for(LineItem item : items){
			total += item.getItemCost();
		}
		return total;
	}
	
	public Date getDate() {
		return date;
	}

	public boolean isFilled() {
		return filled;
	}

	public void setFilled() {
		filled = true;
	}

	@Override
	public String toString() {
		return number;
	}
}
