package lab6;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class OrderService {

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public List<Order> getFilledOrders() {
		List<Order> allOrders = dataSource.getOrders();
		List<Order> filledOrders = new ArrayList<Order>();

		for (Order order : allOrders) {
			if (order.isFilled()) {
				filledOrders.add(order);
			}
		}

		return filledOrders;
	}

	public List<Order> getOrdersOver(double amount) {
		List<Order> allOrders = dataSource.getOrders();
		List<Order> ordersOver = new ArrayList<Order>();

		for (Order order : allOrders) {
			if (order.getTotal() > amount) {
				ordersOver.add(order);
			}
		}
		return ordersOver;
	}

	public List<Order> getOrdersSortedByDate() {
		List<Order> allOrders = dataSource.getOrders();

		java.util.Collections.sort(allOrders, new Comparator<Order>() {

			@Override
			public int compare(Order o1, Order o2) {
				return o1.getDate().compareTo(o2.getDate()) > 0 ? 1 : 0;
			}
		});
		return allOrders;
	}

}
