package lab6;

public class LineItem {

	private String name;
	private double price;
	private int quantity;

	public LineItem(String name, double price, int quantity) {
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public double getAmount() {
		return quantity;
	}

	public double getPrice() {
		return price;
	}

	public double getItemCost() {
		return (double) Math.round(quantity * price * 100) / 100;
	}
}
