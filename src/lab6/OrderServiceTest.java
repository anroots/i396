package lab6;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class OrderServiceTest {

	private OrderService service;

	@Before
	public void before() {
		service = getOrderService();
	}

	@Test
	public void getFilledOrdersReturnsOnlyFilled() {
		List<Order> filledOrders = service.getFilledOrders();

		assertEquals("[N124, N125]", filledOrders.toString());
	}

	@Test
	public void getOrdersOverAmountReturnsMatching() {
		List<Order> ordersOver = service.getOrdersOver(5.0);

		assertEquals("27.2, 12.9", getOrderPricesAsString(ordersOver));
	}

	@Test
	public void sortByDateReturnsSortedOrders() throws Exception {
		List<Order> orders = service.getOrdersSortedByDate();

		assertEquals("01.05.2010, 01.03.2011, 07.05.2011",
				orderDatesAsString(orders));
	}

	/**
	 * Helper method, converts a list of orders into a CSV string with order date values.
	 * @param sortedByDate
	 * @return
	 */
	private String orderDatesAsString(List<Order> sortedByDate) {
		List<String> dates = new ArrayList<String>();
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

		for (Order order : sortedByDate) {
			String date = formatter.format(order.getDate());
			dates.add(date);
		}
		return Util.implodeArray(dates, ", ");
	}

	/**
	 * Helper method, converts a list of orders into a CSV string with order price values.
	 * @param orders
	 * @return
	 */
	private String getOrderPricesAsString(List<Order> orders) {
		List<Double> prices = new ArrayList<Double>();
		for (Order order : orders) {
			prices.add(order.getTotal());
		}
		return Util.implodeArray(prices, ", ");
	}

	/**
	 * Returns OrderService with dummy DataSource (contains test data)
	 * @return
	 */
	private OrderService getOrderService() {

		// Unfinished order, total = 27.2
		final Order order1 = new Order("N123", Util.getDateFromString("2011-05-07"));
		order1.addItem(new LineItem("pen", 0.6, 4));
		order1.addItem(new LineItem("paper", 3.1, 8));

		// Filled order, total = 12.9
		final Order order2 = new Order("N124", Util.getDateFromString("2010-05-01"));
		order2.addItem(new LineItem("ruler", 0.1, 3));
		order2.addItem(new LineItem("cup", 2.1, 6));
		order2.setFilled();

		// Another filled order, total = 4.1
		final Order order3 = new Order("N125", Util.getDateFromString("2011-03-01"));
		order3.addItem(new LineItem("color", 0.1, 9));
		order3.addItem(new LineItem("brush", 3.2, 1));
		order3.setFilled();

		DataSource testDataSource = new DataSource() {

			@Override
			public List<Order> getOrders() {
				return Arrays.asList(order1, order2, order3);
			}

		};

		OrderService service = new OrderService();
		service.setDataSource(testDataSource);

		return service;
	}
}
