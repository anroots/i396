# Harjutstund 6

Tähtaeg 21. nov (23:59)

Harjutuse eesmärk on kasutada testimise mustrit, mis teisendab tulemusd stringiks (5. loeng lk 22-24).
mustri näide on olemas ka kaasasolevas koodis (OrderServiceTest.sortByDateTest()).

Kirjutada testidel põhinevalt (kirjutades testid enne koodi) klass OrderService.
Klassi OrderService meetodid:

List<Order> getFilledOrders()
- tagastab täidetud (filled) tellimused.
  Tulemuste võrdlemiseks tellimuste numbrite string "N123, N456 ..."

List<Order> getOrdersOver(double amount)
- tagastab tellimused, mis ületavad argumendina etteandud summat.
  Tulemuste võrdlemiseks maksumuste (summa üle ridade hind * kogus) string "2.4, 24.8 .."

List<Order> getOrdersSortedByDate()
 - tagastab kõik tellimused kuupäva järgi sorteerituna
   Tulemuste võrdlemiseks kuupäevade string.
   Kuupäeva formaatimise meetodi leiab klassist OrderServiceTest.
   Sorteerimine: java.util.Collections.sort(orders, new MyOrderComparator());
   Vastava komparaatori peate ise tegema.

