package lab6;

import java.util.List;


public interface DataSource {

	List<Order> getOrders();

}
